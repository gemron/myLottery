const {app, BrowserWindow, ipcMain, Tray, Menu} = require('electron');
path = require('path');
url = require('url');
console.log(__dirname);
let mainWindow, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

if (serve) {
  require('electron-reload')(__dirname, {});
}
app.on('ready', function () {
  console.log('Starting application!');
  mainWindow = new BrowserWindow({width: 1280, height: 960});


  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));
  // Opens dev tools
  // mainWindow.webContents.openDevTools();
  mainWindow.on('closed', function () {
    mainWindow = null;
  });


});

app.on('window-all-closed', function () {
  app.quit();
});
