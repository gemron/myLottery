# MyLottery


> 一个基于Electron 和 angular做的抽奖软件，可以编译运行到各种桌面系统,动画效果炫酷，
> 
> 可以选择xlsx格式数据初始化成员和奖项名单，
> 
> data目录为数据文件格式样例，使用时直接选择到目录就可以
> 
> 每次抽奖的结果会自动保存到数据目录的res.txt

使用技术：
- [electron](https://github.com/electron/electron)
- [angular](https://github.com/angular/angular)
- [ng-zorro](https://github.com/NG-ZORRO/ng-zorro-antd)
- [sheetjs](https://github.com/SheetJS/js-xlsx)

希望大家通过此项目可以了解并掌握以上技术，共同进步,共同完善此项目。 :smile: 

有问题直接发issues.

下载：[mylottery-v0.1](https://gitee.com/gemron/myLottery/attach_files/122352/download/myLottery-win32-ia32.7z)

[data示例](https://gitee.com/gemron/myLottery/attach_files/123104/download/data.zip)

win7版本以上系统解压后 运行 myLottery.exe

使用效果：
![效果图](https://gitee.com/uploads/images/2018/0314/141132_15d45587_12082.gif "myLottery1.gif")
## Development

Run `npm install` `npm run starte` for a dev . 

## Build

Run `npm run electron:windows` to build the project for windows. 

## LICENSE

MIT
