import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {NgxElectronModule} from 'ngx-electron';
import {PrizeComponent} from './prize/prize.component';
import {PrizeDoService} from './prize-do.service';
import {EmployeeComponent} from './employee/employee.component';
import {ContainerDirective} from './prize/container.directive';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PrizeComponent,
    EmployeeComponent,
    ContainerDirective
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgZorroAntdModule.forRoot(),
    NgxElectronModule,
    FormsModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}, PrizeDoService],
  bootstrap: [AppComponent],
  entryComponents: [EmployeeComponent]
})
export class AppModule {
}
