import {Injectable} from '@angular/core';

@Injectable()
export class PrizeDoService {

  dataPath: string;
  prizes: any[];

  employees: any[];

  constructor() {
    this.employees = [];
    this.prizes = [];
  }

}
