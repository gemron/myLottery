import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'prize-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  data: any;
  index: number;
  hide: boolean;
  showName = true;

  constructor() {
  }

  ngOnInit() {
  }

  getTop() {
    return Math.floor(this.index / 8) * 42 + 'px';
  }

  getLeft() {
    return this.index % 8 * 124 + 'px';
  }

}
